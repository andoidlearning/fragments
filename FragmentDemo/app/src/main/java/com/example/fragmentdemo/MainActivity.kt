package com.example.fragmentdemo

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.FrameLayout

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val btnFragment1 : Button = findViewById(R.id.btnFragment1)
        val btnFragment2 : Button = findViewById(R.id.btnFragment2)
        val frameLayout : FrameLayout = findViewById(R.id.flFragment)

        val firstFragment = FirstFragment()
        val secondFragment = SecondFragment()

        supportFragmentManager.beginTransaction().apply {
            replace(R.id.flFragment, firstFragment)
            commit()
        } /// set initial fragment to our container

        btnFragment1.setOnClickListener {
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.flFragment, firstFragment)
                addToBackStack(null) // ekade activity stack nahi tya mule back press kele ki app close hote
                // te avoid karnya sathi he kele jate or null la activity name ne replace karun aapan tya activity la move hou shakto
                commit()
            }
        }

        btnFragment2.setOnClickListener {
            supportFragmentManager.beginTransaction().apply {
                replace(R.id.flFragment, secondFragment)
                addToBackStack(null) // ekadun first fragment la janar karan stack made first fragment aahe aani first fragment madun close honar
                commit()
            }
        }
    }
}